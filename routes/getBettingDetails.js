var express = require('express');
var router = express.Router();
//importt web3 library
var Web3 = require("web3");

//Establish connection with local geth private chain
const web3 = new Web3('http://localhost:8545');

// getting data from blockchain
// and rendering it to the auction view
router.get('/', function (req, res, next) {
    //For getting accounts from local geth private chain
    web3.eth.getAccounts().then((data) => {
        console.log("1:"+data);
        console.log("/////",data[0]);
        //call number of bets from deployed contract for getting auction details
        MyContract.methods.numberOfBets().call().then(function (data1) {
            
          
            console.log("2:"+data1);
            //call Total ether bet from deployed contract for getting current state of auction
            MyContract.methods.totalBet().call({ from: data[0] }).then(function (data2) {
                console.log("3:"+data2);
                //call Mycar from deployed contract for getting car details
                MyContract.methods.minimumBet().call({ from: data[0] }).then(function (data3) {
                    
                    wei = web3.utils.fromWei(web3.utils.toBN(data3),'ether');
                   
                   console.log("===============>",wei);
                    
                    MyContract.methods.maxAmountOfBets().call({ from: data[0] }).then(function (data4) {
                        console.log(data4);
                        
                        MyContract.methods.lastWinningNumber().call({ from: data[0] }).then(function (data5) {
                            console.log(data5);

                            //data passing to the auction.ejs
                            res.render("betting", { data:data,data1: data1, data2: data2, wei: wei, data4: data4, data5: data5 });
                        })

                    
                })
            })
        })
    })
})
});
module.exports = router;


