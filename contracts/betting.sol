pragma solidity ^0.5.0;

contract betting {
   address payable public owner;
   uint256 public minimumBet;
   uint256 public totalBet;
   uint256 public numberOfBets;
   uint256 public maxAmountOfBets = 100;
   uint256 public lastWinningNumber;
   address payable[] public players;
   struct Player {
      uint256 amountBet;
      uint256 numberSelected;
   } 
   mapping(address => Player) public playerInfo;

   // event NumberGenerated(uint256 winningNumber);

   constructor(uint _minimumBet) public {
      owner = msg.sender;
      if(_minimumBet != 0 ) minimumBet = _minimumBet;
   }

   // Fallback function in case someone sends ether to the contract so it doesn't get lost
   function() external payable {}

   function kill() public {
      if(msg.sender == owner)
      selfdestruct(owner);
   }

   function checkPlayerExists(address player) public view returns(bool){
      for(uint256 i = 0; i < players.length; i++){
        if(players[i] == player) return true;
     }
     return false;
   }

   // To bet for a number between 1 and 10 both inclusive
   function bet(uint numberSelected) public payable{
    require(!checkPlayerExists(msg.sender));
     require(numberSelected >= 1 && numberSelected <= 10, "Number not between 1 an 10");
     require(msg.value >= minimumBet);
     playerInfo[msg.sender].amountBet = msg.value;
     playerInfo[msg.sender].numberSelected = numberSelected;
     numberOfBets++;
     players.push(msg.sender);
     totalBet += msg.value;
   }

   // Generates a number between 1 and 10
   function generateNumberWinner() public returns(uint256) {
     uint256 numberGenerated = block.number % 10 + 1; 
     lastWinningNumber = numberGenerated;
     distributePrizes(numberGenerated);
   //   emit NumberGenerated(numberGenerated);
   }

   // Sends the corresponding ether to each winner depending on the total bets
   function distributePrizes(uint256 numberWinner) public {
         address payable[100] memory winners; 
         uint256 count = 0; 
         for(uint256 i = 0; i < players.length; i++){
            address payable playerAddress = players[i];
            if(playerInfo[playerAddress].numberSelected == numberWinner){
               winners[count] = playerAddress;
               count++;
            } 
         }
         if(count > 0) {
            uint256 winnerEtherAmount = totalBet / count; // How much each winner gets
            for(uint256 j = 0; j < count; j++){
               if(address(winners[j]) != address(0)) 
               winners[j].transfer(winnerEtherAmount);
            }
         }
         
         resetData();
    }

  // Clears variables of players after prizes are distributed
   function resetData() public {
      players.length = 0; // Delete all the players array
      totalBet = 0;
      numberOfBets = 0;
  }
}
