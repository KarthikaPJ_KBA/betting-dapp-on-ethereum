const betting = artifacts.require("betting");

module.exports = function(deployer) {
  deployer.deploy(betting, web3.utils.toWei("0.1", 'ether'), {gas: 6721975});
};
