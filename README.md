Betting Dapp

* Bet for a number between 1 to 10, Keep a record of how much a user has bet for what number.
* The minimum price of a bet.
* The total amount of ether accumulated.
* A variable to store how many bets there are.
* When to stop the bets and reveal the winning number.
* A function to send the winning amount of ether for each winner.
 

Prerequisites

* Node.js and npm (comes with Node)
* Git
* Geth
* Truffle
* Express generator

Steps

* clone this directory using git clone https://gitlab.com/KarthikaPJ_KBA/betting-dapp-on-ethereum
* cd betting-dapp-on-ethereum
* npm install 
* npm install web3@1.0.0-beta.48 --save
* Run geth privatechain with multiple accounts (Network id: 4002)
* truffle compile
* truffle migrate
* npm start

