// import libraries
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

//import web3 library
var Web3 = require('web3');

//import compiled output of auction contract from build directory
var MyContractJSON  = require(path.join(__dirname, 'build/contracts/betting.json'));



//Establish connection with local geth private chain
web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545")); 



// get contract address, from network id 5777 (here 5777 is the network id of geth private chain)
contractAddress = MyContractJSON.networks['4002'].address; 

//print Contract Address
console.log(contractAddress);

// get abi
const abi = MyContractJSON.abi;

// creating contract object
MyContract = new web3.eth.Contract(abi, contractAddress);

//Then we require() modules from our routes directory.
//These modules/files contain code for handling particular sets of related "routes" (URL paths)
var BettingRouter = require('./routes/getBettingDetails');
var getBetRouter = require('./routes/Bet');
var GenerateWinner = require('./routes/GenerateWinner');
// var withdrawouter = require('./routes/withdrawBid');
// var cancelRouter = require('./routes/cancelBid');

//reate the app object using our imported express module
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//to add the middleware libraries into the request handling chain. 
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//we add our (previously imported) route-handling code to the request handling chain
app.use('/', BettingRouter);
app.use('/Bet', getBetRouter);
 app.use('/GenerateWinner', GenerateWinner);
// app.use('/withdrawBid', withdrawouter);
// app.use('/cancelBid', cancelRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//The Express application object (app) is now fully configured. 
//The last step is to add it to the module exports (this is what allows it to be imported by /bin/www).
module.exports = app;
